using ClassLibrary1;
using System;
using Xunit;

namespace xUnitTryTest
{
    public class UnitTest1
    {
        [Fact]
        public void AddTwoNumbers_OnePlusTwo_ReturnsThree()
        {
            Assert.Equal(3, new MyMath().AddTwoNumbers(1, 2));
        }
    }
}
