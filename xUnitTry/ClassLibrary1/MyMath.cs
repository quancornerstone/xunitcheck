﻿using System;

namespace ClassLibrary1
{
    public class MyMath
    {
        public int AddTwoNumbers(int a, int b) => a + b;
    }
}
